# Microbenchmarking

* Define benchmarks
* Run them
* Export the results
* Analyze them

Last step of the optimization process

Measure in production environment

Source:
* https://www.youtube.com/watch?v=Czr5dBfs72U
* https://www.youtube.com/watch?v=2EWejmkKlxs

## Resulted Assembly code

https://godbolt.org/

## Google Benchmark

https://github.com/google/benchmark

* https://quick-bench.com/

## Nanobench

https://nanobench.ankerl.com/

## Criterion

https://github.com/p-ranav/criterion
