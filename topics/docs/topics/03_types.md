# Types

The size of common types are implementation dependent.

Built-in and user defined types

Arithmetic types (numbers)

`void`: missing information

## Booleans

Implicit boolean conversion

```cpp
int i = true;
```

Explicit boolean conversion
```cpp
bool hasResult {value != 0};
```

Which is better?
```cpp
int* p;
if (p) {}
if (p != nullptr) {}
```

## Characters

ASCII

Unicode

Raw strings

## Integers

`char`, `wchar_t`, `int`, `short`, `long`

```cpp
#include <cstdint>

int64_t count;
```

<!-- TODO: Check all of the standard types! -->

Literal suffixes:
* `L`: long,
* `LL`: long,
* `U`: unsinged.

Lower case is also valid. The order is arbitrary.

(?) How many possible combinations are exists?

```cpp
auto x = 0xABul;
```

<!-- TODO: User defined literal operators -->

```cpp
#include <cstddef>
size_t objSize;
```

## Floating points

<!-- TODO: Railroad diagram for floating point literals -->

```cpp
long double measurement;
```

```cpp
#include <limits>
std::numeric_limits<int>::max();
```

## Pointers

dereferencing/indirection

```cpp
double **weights;
double* weights[N_COLUMNS];
```

<!-- TODO: Function pointers. -->

`void*`: Pointer to an unknown type.

It cannot hold function pointers.

```cpp
int n = 8;
void* p = &n;
int* q = static_cast<int*>(p);
```

```cpp
int n = 8;
void* p = &n;
double* q = static_cast<double*>(p); // unsafe
```

The null pointer

```cpp
long s = nullptr; // invalid
```

```cpp
int* address = NULL; // cant assign
```

Operations
* p + i
* p - i
* ++p
* --p
* q - p

Range for loops does not work without array size.

### Constants

Advantages of named constants
* It helps the readability.
* Update a value all of its usages.
* Requirement for array size, case labels and template types.
* Read-only memory tend to be cheaper.
* Race conditions can be avoided when an expression has been evaluated compile time.
* It sometimes better to calculate in compile time than in runtime.

A constant value always must be initialized.

```cpp
const int errorCode;
```

```cpp
const int values {1, 2, 3, 4};
values[3] = 0; // error
```

Combinations
```cpp
Item* p; // Modifiable pointer to a modifiable item.
const Item* p; // Modifiable pointer to a constant item.
Item const* p; // Constant pointer to a modifiable item.
const Item const* p; // Constant pointer to a constant item.
```

NOTE: The `const*` is the declarator operator! There is no `*const`.

## Arrays

Fixed-length sequence
* No resizing.
* No array assignment.
* No bound checking.

```cpp
int values[10];
int m = values[20]; // undefined behaviour
```

Implicit conversion to pointer type in function arguments.
* C-style programming.
* Causes many problems.

The dynamically allocated arrays must be deallocated by the `delete[]` operator.

`char` array, C-style string
* `strcpy`, `strcmp` functions

Define size explicitly, or by the initialization list:
```cpp
int values[3] = {1, 2, 3};
```

Different sizes:
```cpp
int values[3] = {1, 2, 3, 4}; // error
int values[3] = {1}; // results: {1, 0, 0}
```

<!-- TODO: `sizeof` operator. -->

```cpp
char* text = "something"; // not accepted
text[1] = 'a'; // assigned to const
```

```cpp
char[] text = "something"; // accepted
text[1] = 'a'; // valid
```

String literals are statically allocated, therefore the following code is valid:
```cpp
const char* getMessage() {
    // ...
    return "The message";
}
```

The interning of strings are implementation dependent.

### Raw strings

```cpp
std::string s = R"The \ is allowed here!";
std::string t = R"(Some " marks also possible)";
std::string u = R"--(The "( and )" is also available here.)--";
```

New line characters (multiline strings) are also allowed.

```cpp
std::string content = R"(Some
content
here!)";
std::string content = R"(Some\ncontent\nhere!)";
```

The `r` prefix also can be used.

### Unicode characters

```cpp
std::string a = u8"UTF-8";
std::string a = u"UTF-16";
std::string a = U"UTF-32";
```

It can be combined with raw strings.

```cpp
a[i] == i[a]
```

## References

The reference cannot be void.

|Pointers|References|
|--------|----------|
|Use data without copying|Use data without copying|
|Different syntax for access|same syntax|
|The pointed object can be vary|Refers to the object which used for initialization|
|Can be `nullptr`|Cannot be null|

There is no operator for references. (The operators apply to the referenced object.)

```cpp
int value = 5;
int& ref {value};
int ptr = &ref; // Pointer to the value, not to the reference.
```

There are three kind of references
* lvalue reference: it can be modified
* const lvalue reference: immutable (from the viewpoint of the user)
* rvalue reference: temporary, never be used againg

Rvalues should be used for forwarding and move semantics!

```cpp
std::string message = "Text";

std::string name {message}; // OK (copied)
std::string& name {message}; // OK (lvalue)
std::string&& name {message}; // error (lvalue, cannot be moved)

std::string calcName() {
    return "The Name";
}

std::string name {calcName()}; // OK (copied)
std::string& name {calcName()}; // error (rvalue, temporary)
std::string&& name {calcName()}; // OK (rvalue, moved)

const std::string& temp {"Works properly!"};
```

In collections you can online use pointers.
```cpp
double a, b;

double* ptrs[] = {&a, &b}; // OK
std::vector<double> ptrs = {&x, &y}; // OK

double& refs[] = {a, b}; // error
std::vector<double&> refs = {a, b}; // error
```

Use references where possible!
When it is necessary to denote "missing value", use pointers (with `nullptr`).

## Enumerations

* `enum`: The plain old enum
* `enum class`: Scoped, strongly typed, preferred

```cpp
enum class SignalLevel: int
{
    LOW = 1,
    MEDIUM = 3,
    HIGH = 10
};

SignalLevel level = SignalLevel::MEDIUM;
int i = static_cast<int>(level);
```

### Use for flags

```cpp
enum class StateFlag
{
    READY = 1,
    ACK = 2,
    RST = 4,
    ERROR = 8
};

constexpr StateFlag operator|(StateFlag a, StateFlag b)
{
    return static_cast<StateFlag>(static_cast<int>(a) | static_cast<int>(b));
}

constexpr StateFlag operator&(StateFlag a, StateFlag b)
{
    return static_cast<StateFlag>(static_cast<int>(a) & static_cast<int>(b));
}

bool isAcknowledged(const StateFlag& flag)
{
    return flag | StateFlag::ACK;
}
```

## Type aliases

For more convenient template types
```cpp
template<class T>
class Container {
    using Item = T;
};
```

Can be used instead of the `typedef` macro.

## The `decltype` keyword

## The `issame` keyword

## Casting

Construction

```cpp
double logical = double{true};
```

static
dynamic

*"[...] static cast was designed to be ugly and easy to find in code."* <Stroustrup>

`reinterpret_cast`
`narrow_cast`

C-style casting

Function style cast
```cpp
auto n = int(1.234);
```

## Bit operations

## Structs

```cpp
struct User
{
    unsigned int id;
    std::string name;
};

User user = {
    1234,
    "User Name"
};
```

The size of a structure is not necessarily the sum of the size of the members.
* The compiler align it for more efficient access.
* Order the members for better readability and for following the logic of the application.
* Optimize for alignment only when necessary!
* The access specifiers can modify the layout.

```cpp
alingof(1234);
alingas(T);
```

Self referencing, forward declaration: `examples/03_types/tree_node.cpp`

In C a struct and a non-struct type can have the same name in the same scope.
* These can be distinguished by the `struct` prefix.
* It also apply for `class`, `union` and `enum`.
* This kind of usage should be avoided!

*"The `struct` is a `class` where the members are public default."* <Stroustrup>

## Plain Old Data

Can be copied, moved as a data (by `std::memcpy`).

Requirements:
* simple layout (without virtual pointers),
* does not contain user-defined copy semantics,
* has a trivial default constructor (which does not need to do anything, `=default`).

Can be checked by `std::is_pod<T>` (from `<type_traits>`).

## Bit fields

```cpp
struct Color16
{
    red: 5;
    green: 6;
    blue: 5;
}
```

<!-- TODO: Write IEEE 754 floating point number as an example! -->

## Unions

```cpp
union Number
{
    int i;
    double d;
};

struct Measurement
{
    bool isInteger;
    Number number;
}
```

*"[...] I consider unions an overused feature; avoid them when you can."* <Stroustrup>

It can be solved by inheritance.

```cpp
class Measurement
{
public:
    virtual double getValue() const;
};

class IntMeasurement : public Measurement
{
    virtual double getValue() const
    {
        return _value;
    }
private:
    int _value;
}

// ...
```

## User defined literals
