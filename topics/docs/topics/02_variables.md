# Variables

* Declaration
* Definition
* Initialization

Assignment

Initializers

```cpp
int n = 0;
int n{0};
int n = {0};
int n(0);
```

*The `n{0}` version is recommended by Bjarne Stroustrup*
In this case the narrowing and truncation is not possible (`initializers.cpp`).

```cpp
std::vector<std::string> names{"Works properly."};
std::vector<std::string> names("Not defined.");
```

For default value use empty initializer list.
```cpp
double r {};
```

Implicit/default initialization

Without initializer the
* static objects (global, namespace, local static, static member) are initialized to the default value,
* dynamic objects (local variables, dynamically allocated memory) are not initialized (only with default constructors).

```cpp
int buffer[4096]; // unknown values.
int buffer[4096]{}; // 0 values.
```

## The `auto` keyword

Can be used as a type, when
* the type can be inferred from the right hand side,
* there is a long or complicated type name which decreases the readability.

The `auto` keyword is better for smaller scopes.

```cpp
auto flag = true;
```

The deduced type can be modified by `const` and `&`, for instance:
```cpp
for (const auto& item : items) {
    // ...
}
```

<!-- TODO: Is it a list or initialization list? -->
WARN:
```cpp
auto count {0};
```

```cpp
auto counts {}; // undefined
auto counts {0}; // one element list
```

The type can be deduced for homogeneous lists:
```cpp
auto integers = {1, 2, 3}; // std::initializer_list<int>
auto unknown = {1.0, 2.0, 3};
```

`=` is preferred when using `auto`.

The behavior of the constructor can be vary. For instance `std::vector v(8);` vs `std::vector v{8};`.

<!-- TODO: Is the `auto` adapts to the template type? -->

## Scopes

* local
* class
* namespace
* global
* statement
* function

Scope resolution

```cpp
int n;

void f()
{
    double n = 3;
    std::cout << ::n << std::endl;
}
```

## Lifetimes

* automatic (stack)
* static
* free store (`new`, `delete` operators)
* temporary (expressions, technically mostly automatic)
* thread-local

## Lists

### Qualified lists

Explicitly given type

```cpp
std::vector<int> values {1, 2, 3, 4};
```

### Unqualified lists

The exact type determined from the context.

* function argument
* return value
* assignment (right hand side)
* subscript
