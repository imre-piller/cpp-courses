# Functions

## Callables

Callables which are not functions:
* constructors: no return value, cant have their address
* destructors: unable to overload, cant have their address
* function objects: simply not a function
* lambda functions: a kind of function object

## Returning

* Return statement
* Reaching the end of the function body
* Locally unhandled exception
* Exception has thrown in `noexcept` function
* Invoking system function (`exit`)

## Passing arguments

```cpp
void f(Item a, Item& b, Item* c)
{
    a.name;
    b.name;
    c->name;
}
```

<!-- TODO: Is the reference part of the signature? -->

## Default arguments

```cpp
circle(Point center={}, double radius={}) : center{Point(0, 0)}, radius{0} {
    // ...
}
```

## Unspecified number of argumentes

* `...` (*ellipse*) operator, `<cstdarg>`
* `initializer_list`
* variadic template

## The `const` keyword

```cpp
void f(int* value);
void f(const int* value);
void f(int const* value);
void f(const int const* value);
```

<!-- TODO: Compare its usages! -->

## The `constexpr` keyword

Evaluated at compile time.

It can be
* required in compile time (for instance for constant value, array size, case labels),
* used for better performance.

Can be considered as the restricted form of inline functions.

<!-- TODO: Compare the compilation and runtime by using `constexpr`! -->

## Suffix return type syntax

```cpp
template<type T, type U>
auto operator+(const T& a, const U& b) -> decltype(T{} + U{});
```

## Without return

`[[noreturn]]` attribute

The behaviour of return is undefined.

## Overloading

## Function pointers
