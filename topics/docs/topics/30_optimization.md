# Optimization

* https://qursaan.medium.com/10-essential-tips-for-writing-efficient-and-readable-c-code-c94d8d627810

Requirements of the optimization
* Well tested code.
* Measuring the performance.
* Finding the bottlencks, hotspots.
* Using the proper algorithm. (Try to improve the algorithm itself first.)
* Many assertions for avoiding implementation dependent behavior when porting to other platform.

Deal of performance and readability

Compiler options

Inlining, `constexpr`

Move constructors

Short circuit, probability based order

LRU optimization

Caching, performance vs memory usage

Cache friendly data layout
