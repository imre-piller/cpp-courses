# Profiling

* Define the goals.
* Guarantee that the code is working properly (by tests).
* Measure the actual state.
* Find the bottlenecks, hotspots.
* Try to improve the algorithm first.
* Consider the readability and portability (and further factors).

## GNU Profiler

(`gprof`)

```bash
g++ --std=c++11 -Wall -pg sample.cpp -o sample
./sample
gprof sample gmon.out > results.txt
```

## Valgrind

Callgrind

https://valgrind.org/docs/manual/cl-manual.html

`valgrind --tool=callgrind ./main`

* https://medium.com/@jacksonbelizario/profiling-a-c-program-with-valgrind-callgrind-b41f15b31527
* https://valgrind.org/info/tools.html

## Google Performance Tool

* https://github.com/gperftools/gperftools
* https://developer.ridgerun.com/wiki/index.php/Profiling_with_GPerfTools

## Tracy

https://github.com/wolfpld/tracy
