# Move Semantics

`std::move`, right hand side reference, `&&`

## Swap

```cpp
// Old-style swap
template<class T>
void swap(T& a, T& b)
{
    T temp {a};
    a = b;
    b = temp;
}

// Improved swap by casting
template<class T>
void swap(T& a, T& b)
{
    T temp {static_cast<T&&>(a)};
    a = static_cast<T&&>(b);
    b = static_cast<T&&>(temp);
}

// Improved swap by moving
template<class T>
void swap(T& a, T& b)
{
    T temp {move(a)};
    a = move(b);
    b = move(temp);
}
```

## Perfect forwarding
