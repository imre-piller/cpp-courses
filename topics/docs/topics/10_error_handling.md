# Error Handling

## Common errors

Buffer under or overflow
Value conversions (*value preserving*), implicit conversions
Usage of implementation dependent behavior

Evaluation order
Not defined in the standard. Implementation dependent.
```cpp
values[i] = i--;
```

## Static asserts

```cpp
static_assert(sizeof(long) != 8, "Unexpected long length!");
```

It results compiler error.

## Asserts

# RunTime Type information

RTTI
