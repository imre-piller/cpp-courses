# Const Correctness

## Contant member functions

`const` keyword after argument list.
It guarantees that the call of the member function will not alter the internal state of the object.

```cpp
class Number
{
public:
    int getValue() const
    {
        return _value;
    }
private:
    int _value;
}
```

It cannot invoke non-const member functions.

The `mutable` keyword makes possible to mutate a state from a const member function.

## Indirect access

```cpp
class Indirect
{
    int state;
}

class Number
{
public:
    int getValue() const
    {
        _indirect->state = 1234;
        return _value;
    }
private:
    int _value;
    Indirect* _indirect;
}
```

Literal types
`constexpr` constructors and members
