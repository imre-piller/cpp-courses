# CMake

Sources:
* https://cmake.org/cmake/help/latest/guide/tutorial/index.html
* https://medium.com/@onur.dundar1/cmake-tutorial-585dd180109b
* https://gitlab.kitware.com/cmake/community/-/wikis/home
* https://cmake.org/cmake/help/latest/manual/cmake-commands.7.html
* https://stackoverflow.com/questions/31037882/whats-the-cmake-syntax-to-set-and-use-variables

It is a
* build process manager,
* scripting language.

in-place, out-of-place build

`CMakeLists.txt`

* commands are case insensitive (lower case is preferred officially)
* variables are case sensitive
* Indentation is optional (but advised)

Frequently used commands:
`message`
`cmake_minimum_required`
`add_executable`
`add_library`
`add_subdirectory`

if, endif
elif, endif
while, endwhile
foreach, endforeach
list
return
set_property

## Hello CMake

```cmake
cmake_minimum_required(VERSION 3.10)
project(Hello)
set(CMAKE_CXX_STANDARD 11)
add_executable(hello hello.cpp)
```

Check `examples/24_cmake/01_hello`

For creating the platform specific build configuration:
```bash
cmake .
```

For building the project:
```bash
cmake --build .
```

Running the resulted executable:
```bash
./hello
```

## Variables

```cmake
set(MY_VARIABLE "This!")
message("The value of my variable: ${MY_VARIABLE}")
```

Check `examples/24_cmake/02_variables`

* https://cmake.org/cmake/help/latest/manual/cmake-variables.7.html

Environment variables

* https://cmake.org/cmake/help/latest/manual/cmake-env-variables.7.html

<!-- TODO: Show an example for undef a variable! -->

## Lists

Check `examples/24_cmake/03_lists`

```cmake
set(MY_LIST "First" "Second" "Third")
message("My list: ${MY_LIST}")
```

The separator character is the `;`.

* https://cmake.org/cmake/help/latest/command/list.html

## Conditions

Check `examples/24_cmake/04_conditions`

* https://cmake.org/cmake/help/latest/command/if.html

## Loops

Check `examples/24_cmake/05_loops`

```cmake
set(TXTS file_1.txt file_2.txt file_3.txt)
foreach(TXT ${TXTS})
    message("File name: ${TXT}")
endforeach()
```

* https://cmake.org/cmake/help/latest/command/foreach.html

## Macros and functions

## Detecting the platform

## C++ settings

```cmake
message("CXX Standard: ${CMAKE_CXX_STANDARD}")
set(CMAKE_CXX_STANDARD 14)
```

## Multiple target from the same source

## Creating and linking static library

`STATIC`

## Creating and linking dynamic library

`SHARED`

## Create tests

Test with Google Test

## Add version number to the binary

```bash
./app --version
```

## GUI
