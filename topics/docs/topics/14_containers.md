# Containers

`std::array`

* It has proper object type.
* We cant deduce the array size from the initialization list.

```cpp
std::array<int> numbers = {1, 2, 3};
```

<!-- TODO: Make a detailed comparison of std::array and built-in array! -->

`std::valarray`
`std::vector`

pair

tuple
