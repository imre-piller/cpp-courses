# Templates

* https://blog.devgenius.io/exploring-the-power-of-template-meta-programming-tmp-in-c-72c5ccde9517

## Class templates

## Function templates

## Functors

## Variadic templates


## Template file organization

* https://stackoverflow.com/questions/115703/storing-c-template-function-definitions-in-a-cpp-file
* https://isocpp.org/wiki/faq/templates
* https://www.bogotobogo.com/cplusplus/template_declaration_definition_header_implementation_file.php
* https://softwareengineering.stackexchange.com/questions/373916/c-preferred-method-of-dealing-with-implementation-for-large-templates
