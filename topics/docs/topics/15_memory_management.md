# Memory Management

*free store*, *heap*, *dynamic memory*

Where should we allocate and deallocat?
* In the caller scope?
* In the scope of the called function?

RAII - *Resource Acquisition Is Initialization*
* Allocate memory in the constructor
* Deallocate (free) the memory in the destructor

The temporaries destroyed when its reference goes out of the scope.

```cpp
auto value = new int; // Unknown
auto value = new int {}; // Guaranteed to be 0
```

Problems
* Memory leak
* Premature deletion
* Double deletion

Advice:
* `new` belongs to the constructors,
* `delete` belongs to the destructors.

Consider that an exception can be occur!
```cpp
void calc()
{
    double* x = new double {};
    // Arbitrary code here!
    delete x;
}
```
