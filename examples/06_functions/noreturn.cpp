#include <iostream>

int f() {
    std::cout << "Intentionally there is no return statement here!" << std::endl;
}

int main() {
    int returnValue = f();
    std::cout << "Return value: " << returnValue << std::endl;
    return 0;
}
