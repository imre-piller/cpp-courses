cmake_minimum_required(VERSION 3.10)
project(Lists)

set(MY_LIST "First" "Second" "Third")
message("My list: ${MY_LIST}")
list(LENGTH MY_LIST N_ELEMENTS)
message("Length: " "${N_ELEMENTS}")

list(APPEND MY_LIST "Some" "Further" "Elements")
message("After append: ${MY_LIST}")

list(INSERT MY_LIST 3 "Fourth")
message("After insertion: ${MY_LIST}")

list(FIND MY_LIST "Third" INDEX)
message("Resulted index: ${INDEX}")

list(FILTER MY_LIST INCLUDE REGEX "^F")
message("Remained elements: ${MY_LIST}")
