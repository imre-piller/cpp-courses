#ifndef __COUNTER_H__
#define __COUNTER_H__

#include <set>

class Counter
{
public:
    /**
     * Construct a new counter.
     */
    Counter();

    /**
     * Count the given value.
     */
    void add(int value);

    /**
     * Get the values which has been already counted.
     */
    std::set<int> getValues() const;
};

#endif // __COUNTER_H__
