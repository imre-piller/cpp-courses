#include "counter.h"

#include <gtest/gtest.h>

TEST(CounterTest, Empty)
{
    Counter counter;
    EXPECT_TRUE(counter.getValues().empty());
}

TEST(CounterTest, Single)
{
    Counter counter;
    counter.add(5);
    auto values = counter.getValues();
    EXPECT_EQ(values.size(), 1);
    auto it = values.begin();
    EXPECT_EQ(*it, 5);
}

int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
