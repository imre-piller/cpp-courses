/**
 * $  g++ --std=c++14 main.cpp -o main -lgtest
 */
#include <gtest/gtest.h>

int f(int x)
{
    return 2 * x + 3;
}

TEST(MyFunctionTest, ValidCase)
{
    EXPECT_EQ(f(5), 13);
}

int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
