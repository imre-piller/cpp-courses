#include <iostream>

class Item
{
public:
    Item() {
        std::cout << "Constructed" << std::endl;
    }
    ~Item() {
        std::cout << "Destructed" << std::endl;
    }
};

void f()
{
    std::cout << "Enter the scope." << std::endl;
    Item item;
    std::cout << "Leave the scope." << std::endl;
}

int main()
{
    f();
}
