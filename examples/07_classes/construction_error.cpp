#include <iostream>
#include <stdexcept>

class Item
{
public:
    Item() {
        _content = new int[10];
        throw std::runtime_error("Something strange here!");
        std::cout << "Constructed" << std::endl;
    }
    ~Item() {
        delete [] _content;
        std::cout << "Destructed" << std::endl;
    }
private:
    int* _content;
};

void f()
{
    std::cout << "Enter the scope." << std::endl;
    Item item;
    std::cout << "Leave the scope." << std::endl;
}

int main()
{
    f();
}
