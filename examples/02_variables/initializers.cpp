#include <iostream>
#include <vector>

int main()
{
    double a = 1.0;
    double b {1.0};
    double c = {1.0};

    std::vector<int> values{1, 2, 3};
    std::vector<int> others = {4, 5, 6};

    return 0;
}
