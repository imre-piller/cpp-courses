#include <iostream>
#include <vector>

struct Node
{
    Node* left;
    Node* right;
};

// Forward declaration
struct Item;

struct Container
{
    std::vector<Item> items;
};

struct Item
{
    std::string name;
    Container* parent;
};

int main()
{
    Node node;

    return 0;
}
