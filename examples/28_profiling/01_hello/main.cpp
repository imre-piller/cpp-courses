#include <iostream>

int g(int x)
{
    return x + 3;
}

int f(int x)
{
    if (x % 10 > 0) {
        return 50 * x;
    } else {
        return g(x);
    }
}

int main()
{
    for (int i = 0; i < 1000; ++i) {
        std::cout << f(i) << std::endl;
    }
    return 0;
}
